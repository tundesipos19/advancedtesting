package com.sda.advancedTesting.calculator.exceptions;

//unchecked = RuntimeException
//checked = Exception

public class TruncatedResultException extends RuntimeException{
    public TruncatedResultException(String message){
        super(message);
    }
}
