package com.sda.advancedTesting.calculator;

import com.sda.advancedTesting.calculator.exceptions.TruncatedResultException;

public class Calculator {
    public double add(double a, double b) {
        return a + b;
    }

    public double subtract(double a, double b) {
        return a - b;
    }
    /*
    a * b = ...< Double.MAX_VALUE

     */
    public double multiply(double a, double b) {
        if (a < Double.MAX_VALUE / b) {
            return a * b;
        } else {
            throw new TruncatedResultException("Can not multiply numbers on this size. Result will be truncated");
        }
    }

    public double divide(double a, double b) {
        if (b == 0) {
            throw new IllegalArgumentException("Division by 0 is not supported!");
        } else {
            return a / b;
        }
    }

    public int modulo(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Modulo by 0 is not allowed!");
        }
        return a % b;
    }
    //nu ma obliga sa fac catch
    public void unCheckedE(){
        throw new RuntimeException("This is unchecked exception.");
    }
    //ma obliga sa fac catch
    public void checkedE() throws Exception{
        throw new Exception("This is checked exception.");
    }
}
