package com.sda.advancedTesting.calculator;

import com.sda.advancedTesting.calculator.exceptions.TruncatedResultException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class CalculatorTest {
    @Test
    void verifyAdd(){
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.add(a,b);
        //then
        assertEquals(30,result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifySubtract(){
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.subtract(a,b);
        //then
        assertEquals(10,result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifyMultiply(){
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.multiply(a,b);
        //then
        assertEquals(200,result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifyDivide(){
        //given
        double a = 20;
        double b = 10;
        Calculator calc = new Calculator();
        //when
        double result = calc.divide(a,b);
        //then
        assertEquals(2,result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void verifyModulo(){
        //given
        int a = 20;
        int b = 10;
        Calculator calc = new Calculator();
        //when
        int result = calc.modulo(a,b);
        //then
        assertEquals(0,result);//primul parametru = rezultatul asteptat, al doilea parametru = resultatul returnat de metoda testata
    }

    @Test
    void multiplyIllegalArgumentException(){
        double a = 10;
        double b = Double.MAX_VALUE/(a-1);
        Calculator calculator = new Calculator();
        assertThatExceptionOfType(TruncatedResultException.class)
                .isThrownBy(
                        () -> calculator.multiply(a,b)//metoda lambda(parametrii - daca e cazul) -> {cod}
                );
    }

    @Test
    void divideIllegalArgumentException(){
        double a = 10;
        double b = 0;
        Calculator calc = new Calculator();
         assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(
                        () -> calc.divide(a,b)
                )
                 .withMessage("Division by 0 is not supported!");//mesajul din clasa!!!
    }
    //to do: modulo exception!

}
